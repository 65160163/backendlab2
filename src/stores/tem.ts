import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoad } from './Load'
export const useTem = defineStore('tem', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loading = useLoad()
  type ReturnData = {
    celsius: number
    fahrenheit: number
  }
  function convert(celsius: number): number {
    return (celsius * 9.0) / 5 + 32
  }
  async function callCovert() {
    // result.value = convert(celsius.value)
    loading.doLoad()
    try {
      const res = await http.get(`/temperature/convert?celsius=` + celsius.value)
      const convertRes = res.data as ReturnData
      console.log(convertRes)

      result.value = convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loading.finish()
  }
  return { valid, celsius, result, convert, callCovert }
})
