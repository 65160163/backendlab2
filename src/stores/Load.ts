import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoad = defineStore('load', () => {
  const isLoad = ref(false)

  function doLoad() {
    isLoad.value = true
  }
  function finish() {
    isLoad.value = false
  }

  return { isLoad, doLoad, finish }
})
