import type { User } from '@/types/User'
import http from './http'

function addNew(user: User) {
  return http.post('/users', user)
}

function update(user: User) {
  return http.patch(`/users/${user.id}`, user)
}

function remove(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: Number) {
  return http.get(`/users/${id}`)
}

function getUsers(id: Number) {
  return http.get('/users')
}

export default { addNew, update, remove, getUser, getUsers }
