import { IsEmail, IsNotEmpty } from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  fullname: string;
  // roles: ('admin' | 'user')[];
  gender: 'male' | 'female' | 'other';
}
